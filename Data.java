public class Data {
	private int dia;
	private int mes;
	private int ano;

	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getDia() {
		return dia;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getMes() {
		return mes;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	public int getAno() {
		return ano;
	}

	public String formatada () {
		if (this.dia<10) {
			if(this.mes<10) {
				return "0" + Integer.toString(this.dia) + "/0" + Integer.toString	(this.mes) + "/" + Integer.toString(this.ano);
			} else {
				return "0" + Integer.toString(this.dia) + "/" + Integer.toString	(this.mes) + "/" + Integer.toString(this.ano);
			}
		}
		if(this.mes<10) {
			return Integer.toString(this.dia) + "/0" + Integer.toString(this.mes) + "/" + Integer.toString(this.ano);
		}		
		return Integer.toString(this.dia) + "/" + Integer.toString(this.mes) + "/" + Integer.toString(this.ano);
// Consertei caso o mês for 8 ele saira 08 para ficar bonitinho.
	}
}
