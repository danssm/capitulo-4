
public class Funcionario {
	private String nome;
	private String departamento;
	private double salario;
	private Data dataDeEntrada;
	private String RG;

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getDepartamento() {
		return departamento;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	public double getSalario() {
		return salario;
	}
	
	public void setDataDeEntrada(Data dataDeEntrada) {
		this.dataDeEntrada = dataDeEntrada;
	}
	public String getDataDeEntrada() {
		return dataDeEntrada.formatada();
	}

	public void setRG(String RG) {
		this.RG = RG;
	}
	public String getRG() {
		return RG;
	}

	public void recebeAumento(double aumento) {
		this.salario += aumento;
		System.out.println("Aumento realizado com sucesso!");
		System.out.println();
	}
	
	public double calculaGanhoAnual() {
		double ganhoAnual = 12*salario;		
		return ganhoAnual;
	}

	public void mostra() {
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + this.salario);
		System.out.println("Data de Entrada na Empresa: " + this.dataDeEntrada.formatada());
		System.out.println("RG: " + this.RG);
		System.out.println("Ganho Anual: " + this.calculaGanhoAnual());
	}

}
