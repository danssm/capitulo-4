public class ControleFuncionario {
	public static void main(String[] args) {

		Funcionario umFuncionario1 = new Funcionario();

		umFuncionario1.setNome("Daniel");
		umFuncionario1.setSalario(1000); 
		umFuncionario1.setDepartamento("FGA");

			Data data1 = new Data();
			data1.setDia(19);
			data1.setMes(8);
			data1.setAno(2013);

		umFuncionario1.setDataDeEntrada(data1);	
		umFuncionario1.setRG("981.879");
		umFuncionario1.recebeAumento(1000);
	
		System.out.println("================================");
		System.out.println("Funcionario 1");
		umFuncionario1.mostra();
		System.out.println("================================");
				
		Funcionario umFuncionario2 = new Funcionario();
	
		umFuncionario2.setNome("Daniel");
		umFuncionario2.setSalario(1000);
		umFuncionario2.setDepartamento("FGA");

			Data data2 = new Data();
			data2.setDia(19);
			data2.setMes(8);
			data2.setAno(2013);

		umFuncionario2.setDataDeEntrada(data2);		
		umFuncionario2.setRG("981.879");
		umFuncionario2.recebeAumento(1000);
	
		System.out.println("================================");
		System.out.println("Funcionario 2");
		umFuncionario2.mostra();
		System.out.println("================================");

	            //	Funcionario umFuncionario2 = umFuncionario1;
		if (umFuncionario1 == umFuncionario2) {
			System.out.println();	  			
			System.out.println("==> Funcionário 1 é igual ao Funcionário 2");
		} else {
			System.out.println();		  
			System.out.println("==> Funcionário 1 é diferente de Funcionário 2");    
		}


	}
}
